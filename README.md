# token-breaker

JSON Web token vulnerabilities
tokens are created and signed using a private key, but verified using a corresponding public key
if you publish the public key but keep the private key to yourself, only you can sign tokens, but anyone can check if a given token is correctly signed.

In systems using HMAC signatures, verificationKey will be the server's secret signing key (since HMAC uses the same key for signing and verifying)

In systems using an asymmetric algorithm, verificationKey will be the public key against which the token should be verified

Unfortunately, an attacker can abuse this. If a server is expecting a token signed with RSA, but actually receives a token signed with HMAC, it will think the public key is actually an HMAC secret key.

HMAC secret keys are supposed to be kept private, while public keys are public.
